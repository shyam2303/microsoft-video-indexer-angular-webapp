//Install express server
const express = require('express');
const path = require('path');
const compression = require('compression');
const app = express();


const forceSSL = function () {
  return function (req, res, next) {
    if (req.headers['x-forwarded-proto'] !== 'https') {
      return res.redirect(
        ['https://', req.get('Host'), req.url].join('')
      );
    }
    next();
  }
};
// Serve only the static files form the dist directory
// app.use(cors());
app.use(compression());
app.use(forceSSL());
app.use(express.static(__dirname + '/dist'));

// Start the app by listening on the default Heroku port
app.listen(process.env.PORT || 4200, function () {
  console.log('Listening on port ' + this.address().port); //Listening on port 8888
});
