// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAVNbC7YT1yrvoPC0Gczqqw_lYEKjyVcbQ",
    authDomain: "video-indexer-75ff0.firebaseapp.com",
    databaseURL: "https://video-indexer-75ff0.firebaseio.com",
    projectId: "video-indexer-75ff0",
    storageBucket: "video-indexer-75ff0.appspot.com",
    messagingSenderId: "295792069109"
  },

  API_ENDPOINT: 'https://microsoft-video-indexer-server.herokuapp.com/'
};
