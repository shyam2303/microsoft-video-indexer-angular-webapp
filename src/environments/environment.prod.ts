export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyAVNbC7YT1yrvoPC0Gczqqw_lYEKjyVcbQ",
    authDomain: "video-indexer-75ff0.firebaseapp.com",
    databaseURL: "https://video-indexer-75ff0.firebaseio.com",
    projectId: "video-indexer-75ff0",
    storageBucket: "video-indexer-75ff0.appspot.com",
    messagingSenderId: "295792069109"
  },

  API_ENDPOINT: 'https://microsoft-video-indexer-server.herokuapp.com/'
};
