import { TestBed, inject } from '@angular/core/testing';

import { VideoIndexerService } from './video-indexer.service';

describe('VideoIndexerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VideoIndexerService]
    });
  });

  it('should be created', inject([VideoIndexerService], (service: VideoIndexerService) => {
    expect(service).toBeTruthy();
  }));
});
