import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Http, Headers, RequestOptions } from '@angular/http';
// import { Observable } from '@firebase/util';

import * as firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/take';
import { User } from '../../../core/auth.service';

@Injectable()
export class VideoIndexerService {

  constructor(private httpClient: HttpClient, private http: Http,
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore) { }

  getAccessToken(videoId: string) {
    return this.http.get(`${environment.API_ENDPOINT + 'getAccessToken/'}${videoId}`);
  }

  uploadVideo(formData: FormData, privacy: string, name: string) {

    let params = new HttpParams().set('name', name);
    params = params.append('privacy', privacy);
    params = params.append('language', 'en-US');
    const httpOptions = {
      // headers: new HttpHeaders({
      //   //'Content-Type': 'multipart/form-data',
      //   // 'Content-Type': ''
      // }),
      params: params,
      reportProgress: true
    };
    return this.httpClient.post(environment.API_ENDPOINT + 'uploadVideo', formData, httpOptions);

    // const headers = new Headers();
    // headers.append('Accept', 'application/json');
    // let params = new HttpParams().set('name', name);
    // params = params.append('privacy', privacy);
    // const options = new RequestOptions({ params: params });
    // return this.http.post(environment.API_ENDPOINT + 'uploadVideo', formData, options);
  }

  getProgressState(breakdownId: string) {
    return this.httpClient.get(`${environment.API_ENDPOINT + 'getProcessingState/'}${breakdownId}`);
  }


  updateUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    // userRef.snapshotChanges()
    //   .take(1)
    //   .do(d => {
    //     return userRef.set(user, { merge: true });
    //   });

    return userRef.set(user, { merge: true });
  }

  getBreakdown(breakdownId: string) {
    return this.httpClient.get(`${environment.API_ENDPOINT + 'getBreakdown/'}${breakdownId}`);
  }

  syncData() {

  }

  getPlayerWidgetUrl(breakdownId: string) {
    return this.httpClient.get(`${environment.API_ENDPOINT + 'getPlayerWidgetUrl/'}${breakdownId}`);
  }

  getInsightWidgetUrl(breakdownId: string) {
    return this.httpClient.get(`${environment.API_ENDPOINT + 'getPlayerInsightUrl/'}${breakdownId}`);
  }

  deleteBreakdown(breakdownId: string) {
    return this.httpClient.delete(`${environment.API_ENDPOINT + 'deleteBreakdown/'}${breakdownId}`);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

}
