import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { LandingPageComponent } from './landing-page/landing-page.component';
import { HomeComponent } from './home/home.component';
import { VideoInsightComponent } from './video-insight/video-insight.component';


export const routes: Routes = [
    { path: '', component: LandingPageComponent },
    { path: 'home', component: HomeComponent },
    { path: 'home/:id', component: VideoInsightComponent }];

export const Routing: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true });
