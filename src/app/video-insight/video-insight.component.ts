import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params} from '@angular/router';
import { Location } from '@angular/common';
import { VideoIndexerService } from '../shared/services/video-indexer/video-indexer.service';
import { DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
@Component({
  selector: 'app-video-insight',
  templateUrl: './video-insight.component.html',
  styleUrls: ['./video-insight.component.scss']
})
export class VideoInsightComponent implements OnInit {
  insight:any;
  player:any;
  id = '';
  constructor(private route: ActivatedRoute, private location:Location, private videoIndexerService: VideoIndexerService,private sanitizer: DomSanitizer) {   }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
    });

    this.videoIndexerService.getInsightWidgetUrl(this.id).subscribe(
      (response:any) => {
        this.insight=this.sanitizer.bypassSecurityTrustResourceUrl(response.url);
      }
    );
    this.videoIndexerService.getPlayerWidgetUrl(this.id).subscribe(
      (response:any) => {
        this.player=this.sanitizer.bypassSecurityTrustResourceUrl(response.url);
      }
    );
  }
  back(){
    this.location.back();
  }

}
