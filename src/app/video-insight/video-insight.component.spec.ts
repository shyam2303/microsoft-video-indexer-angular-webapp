import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoInsightComponent } from './video-insight.component';

describe('VideoInsightComponent', () => {
  let component: VideoInsightComponent;
  let fixture: ComponentFixture<VideoInsightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoInsightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoInsightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
