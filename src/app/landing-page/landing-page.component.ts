import { Component, OnInit } from '@angular/core';
import { AuthService } from '../core/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {
  show = false;
  constructor(private auth: AuthService, private router: Router) {
    auth.user.subscribe(user => {

      if (user) {
        window.sessionStorage.setItem('user', JSON.stringify(user));
        this.router.navigate(['/home']);
      }
      else { this.show = true; }
    });
  }

  ngOnInit() {
  }

}
