import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CoreModule } from './core/core.module';

import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { Routing } from './app.routes';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { FileDropModule } from 'ngx-file-drop';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { VideoIndexerService } from './shared/services/video-indexer/video-indexer.service';
import { VideoInsightComponent } from './video-insight/video-insight.component';
import { HeaderComponent } from './header/header.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    HomeComponent,
    VideoInsightComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    CoreModule,
    Routing,
    RouterModule,
    FileDropModule,
    HttpClientModule,
    HttpModule,
    FlexLayoutModule,
    NoopAnimationsModule
  ],
  providers: [
    VideoIndexerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
