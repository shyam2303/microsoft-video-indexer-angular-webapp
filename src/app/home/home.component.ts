import { Component, OnInit } from '@angular/core';
import { UploadEvent, UploadFile } from 'ngx-file-drop';
import { VideoIndexerService } from '../shared/services/video-indexer/video-indexer.service';
import { User } from '../core/auth.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public file: UploadFile;
  formData: FormData;
  privacy = 'Private';
  fileList: FileList;
  selectedFile: File;
  breakdownId = '';
  progressPercentage = 0;
  userData: User;
  breakdownIdArray = [];
  accessTokens = [];
  thumbnailUrls = [];
  isProcessing = false;
  constructor(private videoIndexerService: VideoIndexerService, private snackBar: MatSnackBar) {

  }

  ngOnInit() {
    this.userData = JSON.parse(window.sessionStorage.getItem('user'));

    this.userData.breakdownIds.forEach(id => {
      this.getBreakdown(id);
    });
  }

  public dropped(event: UploadEvent) {
    this.file = event.files[0];
    if (this.file.fileEntry.isFile) {
      const fileEntry = this.file.fileEntry as FileSystemFileEntry;
      fileEntry.file((file: File) => {

        if (this.validateFile(this.file)) {
          this.formData = new FormData();
          this.formData.append('avatar', file, this.file.relativePath);
          this.uploadVideo(this.formData, this.privacy, this.file.relativePath);
        }
      });
    }

    else {
      this.displayMessage('Please an mp4 or ogg video file', 'Invalid Data');
    }

  }

  initFileUpload(evt: any) {
    this.fileList = evt.target.files;
    if (this.fileList.length > 0) {
      this.selectedFile = this.fileList[0];
      if (this.validateFile(this.selectedFile)) {
        this.formData = new FormData();
        this.formData.append('avatar', this.selectedFile, this.selectedFile.name);
        this.uploadVideo(this.formData, this.privacy, this.selectedFile.name);
      }

      else {
        this.displayMessage('Please add a mp4 or ogg video file', 'Invalid Data');
      }
    }
  }


  uploadVideo(formData: FormData, privacy: string, fileName: string) {
    this.isProcessing = true;
    this.videoIndexerService.uploadVideo(formData, privacy, fileName).subscribe(
      (response: any) => {

        this.breakdownId = response.breakdownId;
        this.getProgressState(this.breakdownId);
      },
      error => {

      }
    );
  }


  returnFileSize(number) {
    if (number < 1024) {
      return number + 'bytes';
    } else if (number > 1024 && number < 1048576) {
      return (number / 1024).toFixed(1) + 'KB';
    } else if (number > 1048576) {
      return (number / 1048576).toFixed(1) + 'MB';
    }
  }

  validateFile(data: any): boolean {
    if (data.type === 'video/mp4' || data.type === 'video/ogg') {
      if (data.size > 1e+7 * 3) {
        this.displayMessage('Please add a file less than 30 Mb', 'Exceeded file size limit');
        return false;
      }
      else {

        return true;
      }
    }
    else {
      this.displayMessage('Please add a mp4ds or ogg video file', 'Invalid file type');
      return false;

    }

  }

  getProgressState(breakdownId: string) {
    this.videoIndexerService.getProgressState(breakdownId).subscribe(
      (response: any) => {


        if (response.progress.state === 'Uploaded') {
          this.progressPercentage = 0;
          setTimeout(() => {
            this.getProgressState(this.breakdownId);
          }, 20000
          );

        }
        else if (response.progress.state === 'Processing') {
          this.progressPercentage = +response.progress.progress.substring(0, response.progress.progress.length - 1);
          setTimeout(() => {
            this.getProgressState(this.breakdownId);
          }, 20000
          );
        }
        else if (response.progress.state === 'Processed') {
          this.progressPercentage = 100;
          this.userData.breakdownIds.push(this.breakdownId);
          this.displayMessage('Video Added Successfully', 'ADDED');
          this.getBreakdown(this.breakdownId);
          this.isProcessing = false;
          this.videoIndexerService.updateUserData(this.userData).then(res => {

            window.sessionStorage.setItem('user', JSON.stringify(this.userData));
          }).catch(error => {

          });
        }

      },
      error => {

      }
    );
  }

  getBreakdown(breakdownId: string) {
    this.videoIndexerService.getBreakdown(breakdownId).subscribe(
      (response: any) => {

        const urlData: any = {};
        urlData.id = response.id;
        urlData.thumbnailUrl = response.summarizedInsights.thumbnailUrl;
        urlData.durationInSeconds = response.durationInSeconds;
        urlData.name = response.name;
        urlData.createTime = response.createTime;
        urlData.views = response.social.views;
        this.thumbnailUrls.push(urlData);

      });
  }

  deleteBreakdown(breakdownId: string, index: number) {
    return this.videoIndexerService.deleteBreakdown(breakdownId).subscribe(
      response => {

        this.thumbnailUrls.splice(index, 1);
        this.userData.breakdownIds.splice(index, 1);
        this.videoIndexerService.updateUserData(this.userData).then(res => {

          this.displayMessage('Video Deleted Successfully', 'DELETED');
          window.sessionStorage.setItem('user', JSON.stringify(this.userData));
        }).catch(error => {

        });
      },
      error => {

      }
    );
  }

  displayMessage(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
      direction: 'ltr',
      verticalPosition: 'top'
    });
  }
}


export interface FileSystemEntry {
  name: string;
  isDirectory: boolean;
  isFile: boolean;
}
export interface FileSystemEntryMetadata {
  modificationTime?: Date;
  size?: number;
}
export interface FileSystemDirectoryReader {
  readEntries(successCallback: (result: FileSystemEntry[]) => void, errorCallback?: (error: DOMError) => void): void;
}
export interface FileSystemFlags {
  create?: boolean;
  exclusive?: boolean;
}
export interface FileSystemDirectoryEntry extends FileSystemEntry {
  isDirectory: true;
  isFile: false;
  createReader(): FileSystemDirectoryReader;
  getFile(path?: USVString, options?: FileSystemFlags, successCallback?: (result: FileSystemFileEntry) => void, errorCallback?: (error: DOMError) => void): void;
  getDirectory(path?: USVString, options?: FileSystemFlags, successCallback?: (result: FileSystemDirectoryEntry) => void, errorCallback?: (error: DOMError) => void): void;
}
export interface FileSystemFileEntry extends FileSystemEntry {
  isDirectory: false;
  isFile: true;
  file(callback: (file: File) => void): void;
}
